#/usr/bin/env bash

# Directory of PACKAGES
declare -r FILE_PACKAGE="$1"

# File SSH_PRIVATE_KEY
declare -r SSH_PRIVATE_KEY="$2"

# Usage
if [[ $# -lt 1 ]] ; then
cat << END
You missed some attributes:
  FILE_PACKAGE: path of the listing of the packages
  SSH_PRIVATE_KEY: path of your ssh key for AUR (default ~/.ssh/id_rsa)
END
  exit 1
fi

# Ssh-agent is launch
if pstree | grep 'ssh-agent' > /dev/null ; then
  ssh-add ${SSH_PRIVATE_KEY}

  # Error when we tried add private key
  if [[ $? != '0' ]] ; then
    printf 'Error when we tried to add private key\n'
    exit 1
  fi
else
  printf 'Ssh-agent is not launch\n'
  printf 'You may try to launch it with "eval `ssh-agent -s`"\n'
  exit 1
fi

if test -d ./AUR_REPO/ ; then
  printf 'Directory AUR_REPO exist\n'
  printf 'Remove this directory and launch the clone-aur-repo.sh again\n'
  exit 1
fi

while read PACKAGES ; do
  if [[ ! -z "$PACKAGES" ]] ; then
    git clone ssh://aur@aur.archlinux.org/${PACKAGES}.git ./AUR_REPO/${PACKAGES}/
  fi
done < ${FILE_PACKAGE}

# Message to remove the SSH_PRIVATE_KEY from the ssh-agent
printf '/!\ Forgot not to kill your ssh-agent or remove your stored key /!\\\n'
printf 'Remove all identities with "ssh-add -D"\n'

exit 0
