#!/usr/bin/env bash

if [[ "$#" -lt 1 ]] ; then
  cat << END
You missed some attribute:
  VERSION-RELEASE: the version-release to set un the commit message
  PUSH: if no it not push to AUR server
  MESSAGE (optionnal) : message for commit
END
  exit 1
fi

# Define VERSION_RELEASE
declare -r VERSION_RELEASE="$1"
declare -r PUSH="$2"
declare -r MESSAGE="$3"

if [[ -z $(echo $(whereis mksrcinfo | cut -d ':' -f2)) ]] ; then
  printf 'Install pkgbuild-introspection\n'
  printf 'Forgot not to configure your git\n'
  printf 'git config --global user.email "you@example.com"\n'
  printf 'git config --global user.name "Your Name"\n'
  exit 1
fi

while read DIRECTORY ; do
  cd $DIRECTORY

  # Commit an updated package
  mksrcinfo

  if [[ -z "$MESSAGE" ]] ; then
    git commit -am "Update to ${VERSION_RELEASE}"
  else
    git commit -am "$MESSAGE"
  fi

  # Push the repo
  if [[ "$PUSH" != 'no' ]] ; then
    git push
  fi

  # Return to the old directory
  cd ../../
done < <(find ./AUR_REPO/ -mindepth 1 -maxdepth 1 -type d)

exit 0

