#!/usr/bin/env bash

# Usage
if [[ "$#" -lt 3 ]] ; then
  cat << END
You missed some attributes:
  VERSION-RELEASE: new version and release for the PKGBUILD
  MD5_FD: the MD5 for fusiondirectory.tar.gz
  MD5_FDPLUGINS: the MD5 for fusiondirectory-plugins.tar.gz
END
  exit 1
fi

# New version
declare -r VERSION="$(echo $1 | cut -d '-' -f1)"

# New release
declare -r RELEASE="$(echo $1 | cut -d '-' -f2)"

# Define MD5 for fusiondirectory.tar.gz
declare -r MD5_FD="$2"

# Define MD5 for fusiondirectory-plugins.tar.gz
declare -r MD5_FDPLUGINS="$3"

# Replace pkgver in PKGBUILD
find ./AUR_REPO/ -type f -name PKGBUILD -exec sed -i "s/^pkgver=.*$/pkgver=${VERSION}/g" {} \;

# Replace pkgrel in PKGBUILD
find ./AUR_REPO/ -type f -name PKGBUILD -exec sed -i "s/^pkgrel=.*$/pkgrel=${RELEASE}/g" {} \;

# Replace MD5 in PKGBUILD for fusiondirectory.tar.gz
while read FILE ; do
  if echo "$FILE" | grep 'shell' > /dev/null ; then
    sed -i "s/^md5sums=.*$/md5sums=('${MD5_FDPLUGINS}')/g" "$FILE"
  else
    sed -i "s/^md5sums=.*$/md5sums=('${MD5_FD}'/g" "$FILE"
  fi
done < <(find ./AUR_REPO/ -type f -name PKGBUILD | grep -v 'plugin')

# Replace MD5 in PKGBUILD for fusiondirectory-plugins.tar.gz
while read FILE ; do
  sed -i "s/^md5sums=.*$/md5sums=('${MD5_FDPLUGINS}' '${MD5_FD}')/g" "$FILE"
done < <(find ./AUR_REPO/ -type f -name PKGBUILD | grep 'plugin')

exit 0
